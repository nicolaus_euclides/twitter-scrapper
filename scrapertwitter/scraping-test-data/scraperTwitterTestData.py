import time
import csv
import pandas as pd

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

browser = webdriver.Chrome()
base_Url = u'https://twitter.com/search?q='

query = u'jokowi'
url = base_Url + query

browser.get(url)
time.sleep(2)

body = browser.find_element_by_tag_name('body')

for _ in range(10):
    body.send_keys(Keys.PAGE_DOWN)
    time.sleep(2)

search_results = browser.find_elements_by_xpath("//div[@class='content']")

scraped_data = []
tweet = []
for search_result in search_results:
    tweets = search_result.find_elements_by_class_name('tweet-text')
    for tweeti in tweets:
        tweet = tweeti.text
    scraped_data.append(tweet)  # put in tuples

df = pd.DataFrame(data=scraped_data, columns=["Tweet"])
df.to_csv("data-test-tweet-jokowi.csv")

browser.quit()
